# Finesse tools for WordPress
Finesse WordPress block.

## Usage

### Enabling SVG plots
Plots using the SVG file format used if the user is allowed to upload them.
By default, WordPress blocks SVG uploads (even for administrators) due to the
ability to embed potentially harmful JavaScript in them. Installing a plugin
like [Safe SVG](https://wordpress.org/plugins/safe-svg/) will allow SVGs to be
uploaded and if that is the case they will be used by this plugin.

## Credits
Sean Leavey <sean.leavey@ligo.org>