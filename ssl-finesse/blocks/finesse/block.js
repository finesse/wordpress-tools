/**
 * Finesse block.
 *
 * This block allows Finesse plots to be generated and displayed.
 *
 * @package ssl-finesse
 */

( function( wp ) {
	/**
	 * New block registration function.
	 */
    var registerBlockType = wp.blocks.registerBlockType;

    /**
     * Data selector.
     */
    var withSelect = wp.data.withSelect;

	/**
	 * Elements.
	 */
    var el = wp.element.createElement;
    var Component = wp.element.Component;
    var Fragment = wp.element.Fragment;

    /**
     * Components.
     */
    var Button = wp.components.Button;
    var Placeholder = wp.components.Placeholder;
    var Spinner = wp.components.Spinner;

    /**
     * Editor stuff.
     */
    var BlockControls = wp.editor.BlockControls;
    var BlockAlignmentToolbar = wp.editor.BlockAlignmentToolbar;

	/**
	 * Retrieves the translation of text.
	 *
	 * @see https://github.com/WordPress/gutenberg/tree/master/i18n#api
	 */
    var __ = wp.i18n.__;

	const blockIcon = el(
		'svg',
		{
			width: 24,
			height: 24,
		},
		el(
			'path',
			{
				d: "M17.882768 15.114406C18.862527 15.114406 19.65678 15.908659 19.65678 16.888418C19.65678 16.888418 19.65678 18.662429 19.65678 18.662429C19.65678 19.642188 18.862527 20.436441 17.882768 20.436441C17.882768 20.436441 16.108756 20.436441 16.108756 20.436441C15.128998 20.436441 14.334745 19.642188 14.334745 18.662429C14.334745 18.662429 14.334745 16.888418 14.334745 16.888418C14.334745 15.908659 15.128998 15.114406 16.108756 15.114406C16.108756 15.114406 16.108756 13.340394 16.108756 13.340394C16.108756 13.340394 7.2387006 13.340394 7.2387006 13.340394C7.2387006 13.340394 7.2387006 15.114406 7.2387006 15.114406C8.2184598 15.114406 9.0127117 15.908659 9.0127117 16.888418C9.0127117 16.888418 9.0127117 18.662429 9.0127117 18.662429C9.0127117 19.642188 8.2184598 20.436441 7.2387006 20.436441C7.2387006 20.436441 5.4646893 20.436441 5.4646893 20.436441C4.48493 20.436441 3.6906782 19.642188 3.6906782 18.662429C3.6906782 18.662429 3.6906782 16.888418 3.6906782 16.888418C3.6906782 15.908659 4.48493 15.114406 5.4646893 15.114406C5.4646893 15.114406 5.4646893 13.340394 5.4646893 13.340394C5.4646893 12.360636 6.2589412 11.566383 7.2387006 11.566383C7.2387006 11.566383 10.786723 11.566383 10.786723 11.566383C10.786723 11.566383 10.786723 9.7923727 10.786723 9.7923727C9.8069639 9.7923727 9.0127117 8.9981209 9.0127117 8.0183616C9.0127117 8.0183616 9.0127117 6.2443504 9.0127117 6.2443504C9.0127117 5.2645911 9.8069639 4.4703393 10.786723 4.4703393C10.786723 4.4703393 12.560734 4.4703393 12.560734 4.4703393C13.540494 4.4703393 14.334745 5.2645911 14.334745 6.2443504C14.334745 6.2443504 14.334745 8.0183616 14.334745 8.0183616C14.334745 8.9981209 13.540494 9.7923727 12.560734 9.7923727C12.560734 9.7923727 12.560734 11.566384 12.560734 11.566384C12.560734 11.566384 16.108756 11.566384 16.108756 11.566384C17.088516 11.566384 17.882768 12.360636 17.882768 13.340394C17.882768 13.340394 17.882768 15.114406 17.882768 15.114406M5.4646893 16.888418C5.4646893 16.888418 5.4646893 18.662429 5.4646893 18.662429C5.4646893 18.662429 7.2387006 18.662429 7.2387006 18.662429C7.2387006 18.662429 7.2387006 16.888418 7.2387006 16.888418C7.2387006 16.888418 5.4646893 16.888418 5.4646893 16.888418M16.108756 16.888418C16.108756 16.888418 16.108756 18.662429 16.108756 18.662429C16.108756 18.662429 17.882768 18.662429 17.882768 18.662429C17.882768 18.662429 17.882768 16.888418 17.882768 16.888418C17.882768 16.888418 16.108756 16.888418 16.108756 16.888418M10.786723 6.2443504C10.786723 6.2443504 10.786723 8.0183616 10.786723 8.0183616C10.786723 8.0183616 12.560734 8.0183616 12.560734 8.0183616C12.560734 8.0183616 12.560734 6.2443504 12.560734 6.2443504C12.560734 6.2443504 10.786723 6.2443504 10.786723 6.2443504",
            }
		),
    );

    class KatImage extends Component {
        render() {
            const { imageSrc } = this.props;

            return el(
                'img',
                {
                    className: 'ssl-finesse-kat-plot-img',
                    src: imageSrc || '',
                }
            );
        }
    }

    /**
     * Kat tab panel used to display the Finesse plot and script.
     */
    class KatTabPanel extends Component {
        getPlotWidget() {
            const { align, updateAlignment, script, imageSrc, imageUrl, editMode, gettingPlot, failedToGetPlot, fetchMedia, placeholderText } = this.props;

            if ( editMode ) {
                var imgOrPlaceholder;

                const controls = el(
                    BlockControls,
                    {},
                    el(
                        BlockAlignmentToolbar,
                        {
                            value: align,
                            onChange: updateAlignment,
                        },
                    )
                );

                if ( imageSrc ) {
                    imgOrPlaceholder = el(
                        KatImage,
                        {
                            imageSrc,
                        }
                    );
                } else {
                    var placeholderItem;

                    if ( script ) {
                        if ( failedToGetPlot ) {
                            // Show button to start getting plot again.
                            placeholderItem = el(
                                Button,
                                {
                                    className: 'components-button is-button', // Borrwed from Gutenberg
                                    onClick: () => {
                                        fetchMedia();
                                    },
                                },
                                __( 'Try Again', 'ssl-finesse' )
                            )
                        } else if ( gettingPlot ) {
                            // Show spinner while we wait for the plot to be retrieved.
                            placeholderItem = Spinner();
                        } else {
                            // IS THIS EVER USED? e.g. on startup

                            placeholderItem = el(
                                Button,
                                {
                                    className: 'components-button is-button', // Borrwed from Gutenberg
                                    onClick: () => {
                                        fetchMedia();
                                    },
                                },
                                __( 'Run', 'ssl-finesse' )
                            )
                        }
                    } else {
                        placeholderItem = el(
                            'p',
                            {},
                            __( 'No script entered.', 'ssl-finesse' )
                        );
                    }

                    imgOrPlaceholder = el(
                        Placeholder,
                        {
                            className: 'ssl-finesse-kat-plot-placeholder',
                            label: placeholderText,
                        },
                        placeholderItem
                    );
                }
                
                return el(
                    Fragment,
                    {},
                    controls,
                    imgOrPlaceholder
                );
            } else {
                const img = el(
                    KatImage,
                    {
                        imageSrc,
                    }
                );

                if ( imageUrl ) {
                    return el(
                        'a',
                        {
                            className: 'ssl-finesse-kat-attachmenturl',
                            href: imageUrl,
                        },
                        img
                    );
                }

                return img;
            }
        }

        getScriptEditorWidget() {
            const { script, editMode, fetchMedia, onKatScriptChange } = this.props;

            if ( editMode ) {
                var controls;

                if ( fetchMedia ) {
                    controls = el(
                        'div',
                        {
                            className: 'ssl-finesse-kat-script-input-controls',
                        },
                        el(
                            Button,
                            {
                                className: 'ssl-finesse-kat-script-input-controls-run components-button is-button', // Borrowed from Gutenberg
                                onClick: fetchMedia,
                            },
                            __( 'Run', 'ssl-finesse' )
                        )
                    );
                }

                return el(
                    Fragment,
                    {},
                    el(
                        'textarea',
                        {
                            className: 'ssl-finesse-kat-script-input ssl-finesse-kat-script-input-edit',
                            name: 'textarea1',
                            onChange: ( editMode ) ? onKatScriptChange : null,
                            value: script,
                            spellCheck: false,
                            placeholder: ( editMode ) ? __( 'Enter kat script', 'ssl-finesse' ) : null,
                            rows: 15,
                        }
                    ),
                    //controls
                );
            }

            // Front end rendering.
            return el(
                'pre',
                {
                    className: 'ssl-finesse-kat-script-input ssl-finesse-kat-script-input-display',
                },
                el(
                    'code',
                    {},
                    script
                )
            );
        }

        render() {
            const { align, imageWidth, imageHeight, selectedTab = 'plot' } = this.props;

            return el(
                'div',
                {
                    className: 'ssl-finesse-kat-tab-panel' + ( ( align ) ? ` align${ align }` : '' ),
                    style: {
                        // Set the container to exactly hold the image's dimensions. This ensures the
                        // overall tab panel dimensions remain when switched to the script tab.
                        width: `${ imageWidth }px`,
                        //maxWidth: `${ imageWidth }px`, // max-width so that doesn't overflow on block editor
                    },
                    'data-img-width': imageWidth,
                    'data-img-height': imageHeight,
                },
                el(
                    Fragment,
                    {},
                    // Tabs container.
                    el(
                        'div',
                        {
                            className: 'ssl-finesse-kat-tab-panel-tab-container',
                            style: {
                                height: `${ imageHeight }px`, // Set tab contents height to that of image.
                            }
                        },
                        el(
                            Fragment,
                            {},
                            el(
                                'div',
                                {
                                    className: 'ssl-finesse-kat-tab-panel-tab' + ( ( 'plot' == selectedTab ) ? ' ssl-finesse-kat-tab-panel-tab-selected' : '' ),
                                    'data-tab': 'plot',
                                },
                                el(
                                    'div',
                                    {
                                        className: 'ssl-finesse-kat-plot',
                                    },
                                    this.getPlotWidget()
                                )
                            ),
                            el(
                                'div',
                                {
                                    className: 'ssl-finesse-kat-tab-panel-tab' + ( ( 'script' == selectedTab ) ? ' ssl-finesse-kat-tab-panel-tab-selected' : '' ),
                                    'data-tab': 'script',
                                },
                                el(
                                    'div',
                                    {
                                        className: 'ssl-finesse-kat-script',
                                    },
                                    this.getScriptEditorWidget()
                                )
                            )
                        )
                    ),
                    // Tab links
                    el(
                        'ul',
                        {
                            className: 'ssl-finesse-kat-tab-panel-tabs',
                        },
                        el(
                            Fragment,
                            {},
                            el(
                                'li',
                                {
                                    className: 'ssl-finesse-kat-tab-panel-tab-link' + ( ( 'plot' == selectedTab ) ? ' ssl-finesse-kat-tab-panel-tab-link-selected' : '' ),
                                    'data-tab': 'plot',
                                },
                                __( 'Plot', 'ssl-finesse' )
                            ),
                            el(
                                'li',
                                {
                                    className: 'ssl-finesse-kat-tab-panel-tab-link' + ( ( 'script' == selectedTab ) ? ' ssl-finesse-kat-tab-panel-tab-link-selected' : '' ),
                                    'data-tab': 'script',
                                },
                                __( 'Script', 'ssl-finesse' )
                            )
                        )
                    )
                )
            );
        }
    }

    function getEditComponent( blockName, blockTitle ) {
        return class extends Component {
            constructor( props ) {
                super( props );

                this.state = {
                    image: null,
                    jobId: null,
                    gettingPlot: false,
                    failedToGetPlot: false,
                };
            }

            componentDidMount() {
                this.mounted = true;

                // Fetch/show existing media, if any.
                this.fetchMedia();
            }

            componentWillUnmount() {
                this.mounted = false;
            }

            onKatScriptChange( event ) {
                const { setAttributes } = this.props;

                // Update script attribute as it is typed.
                setAttributes(
                    {
                        script: event.target.value,
                        imageSrc: null,
                        imageUrl: null,
                    }
                );

                if ( ! this.mounted ) {
                    return;
                }

                // Unset existing media attribute.
                this.setState(
                    {
                        image: null,
                        jobId: null,
                    }
                );
            }

            updateAlignment( nextAlign ) {
                const { setAttributes } = this.props;

                setAttributes(
                    {
                        align: nextAlign,
                    }
                );
            }

            fetchMedia() {
                const { image, gettingPlot } = this.state;
                const { attributes, setAttributes } = this.props;
                const { script, currentPostId } = attributes;

                if ( image != null || gettingPlot || ! script ) {
                    console.log("existing image, or getting plot, or no script");
                    return;
                }

                this.setState(
                    {
                        gettingPlot: true,
                        failedToGetPlot: false,
                    }
                );

                const sleep = ( time ) => {
                    return new Promise( ( resolve ) => setTimeout( resolve, time ) );
                }

                const awaitFinishedJob = ( params, retries = 5, delay = 1000 ) => {
                    return new Promise( ( resolve, reject ) => {
                        const doFetch = ( retries, delay ) => {                            
                            wp.apiFetch( params ).then(
                                ( response ) => {
                                    if ( 'finished' === response.status ) {
                                        resolve( response );
                                    } else {
                                        if ( retries <= 1 ) {
                                            return reject();
                                        }

                                        sleep( delay * 2 ).then( () => {
                                            doFetch( retries - 1, delay * 2 );
                                        } );
                                    }
                                }
                            );
                        }

                        doFetch( retries, delay );
                    } );
                }

                const fetchRemotePlot = ( jobId ) => {
                    wp.apiFetch(
                        {
                            path: '/ssl-finesse/v1/kat-plot?job_id=' + jobId + "&post_id=" + currentPostId,
                            method: 'GET',
                        }
                    ).then(
                        ( attachment ) => {
                            if ( ! this.mounted ) {
                                return;
                            }

                            console.log("attachment:");
                            console.log(attachment);

                            const mediaDetails = attachment.media_details;
                            const imgData = mediaDetails.sizes.full;

                            setAttributes(
                                {
                                    attachmentId: attachment.id,
                                    imageSrc: imgData.source_url,
                                    imageUrl: imgData.source_url,
                                    // Use media details instead of a particular size's
                                    // dinensions to get the width/height because this works
                                    // for both PNGs and SVGs.
                                    imageWidth: mediaDetails.width,
                                    imageHeight: mediaDetails.height,
                                }
                            );

                            this.setState(
                                {
                                    gettingPlot: false,
                                    image: attachment,
                                }
                            );
                        }
                    ).catch(
                        ( error ) => {
                            console.log("error with plot:");
                            console.log( error );

                            this.setState(
                                {
                                    gettingPlot: false,
                                    failedToGetPlot: true,
                                }
                            );
                        }
                    )
                }

                // Get kat job ID.
                wp.apiFetch(
                    {
                        path: '/ssl-finesse/v1/kat-run',
                        method: 'POST',
                        data: {
                            script: script,
                        },
                    }
                ).then(
                    ( reply ) => {
                        if ( ! this.mounted ) {
                            return;
                        }

                        console.log(reply)
                        const newJobId = reply.job.uuid;

                        this.setState(
                            {
                                jobId: newJobId,
                            }
                        );

                        if ( 'finished' === reply.job.status ) {
                            // Job is already finished.
                            fetchRemotePlot( newJobId );
                        } else {
                            // Wait until job is finished.
                            awaitFinishedJob(
                                {
                                    path: '/ssl-finesse/v1/kat-info?job_id=' + newJobId,
                                    method: 'GET',
                                }
                            ).then(
                                ( reply ) => {
                                    fetchRemotePlot( reply.uuid )
                                }
                            ).catch(
                                ( error ) => {
                                    console.log("error with info:");
                                    console.log( error );
            
                                    this.setState(
                                        {
                                            gettingPlot: false,
                                            failedToGetPlot: true,
                                        }
                                    );
                                }
                            );
                        }
                    }
                ).catch(
                    ( error ) => {
                        console.log("error with job fetch:");
                        console.log( error );

                        this.setState(
                            {
                                gettingPlot: false,
                                failedToGetPlot: true,
                            }
                        );
                    }
                );
            }

            render() {
                const { gettingPlot, failedToGetPlot } = this.state;
                const { attributes } = this.props;
                const { align, script, imageSrc, imageWidth, imageHeight } = attributes;

                return el(
                    KatTabPanel,
                    {
                        editMode: true,
                        selectedTab: 'plot',
                        align,
                        updateAlignment: ( nextAlign ) => {
                            this.updateAlignment( nextAlign );
                        },
                        script,
                        imageSrc,
                        imageWidth,
                        imageHeight,
                        gettingPlot,
                        failedToGetPlot,
                        fetchMedia: () => {
                            this.fetchMedia()
                        },
                        onKatScriptChange: ( event ) => {
                            this.onKatScriptChange( event );
                        },
                        placeholderText: blockTitle,
                    }
                );
            }
        };
    };

    function save( { attributes } ) {
        const { align, script, imageSrc, imageUrl, imageWidth, imageHeight } = attributes;

        return el(
            KatTabPanel,
            {
                editMode: false,
                align,
                script,
                imageSrc,
                imageUrl,
                imageWidth,
                imageHeight,
            }
        );
    }

    const name = 'ssl-finesse/finesse-plot';
    const title = __( 'Finesse Plot', 'ssl-finesse' );
    const edit = getEditComponent( name, title );

	registerBlockType( name, {
		title: title,

		description: __( 'Displays a Finesse plot.', 'ssl-finesse' ),

		keywords: [
            __( 'kat', 'ssl-finesse' ),
            __( 'run', 'ssl-finesse' ),
            __( 'figure', 'ssl-finesse' ),
		],

		icon: blockIcon,

        category: 'widgets',
        
        attributes: {
            // Attachment post ID.
			attachmentId: {
				type: 'number',
            },
            // Image alignment.
            // Note that the core image block uses a stored attribute here, not one
            // parsed from a property - we're doing the same just to be safe.
            align: {
                type: "string",
            },
            // Kat script.
            script: {
                type: 'string',
                source: 'text',
                selector: 'pre.ssl-finesse-kat-script-input-display code',
            },
            // Image source URL.
            imageSrc: {
                type: 'string',
                source: 'attribute',
                selector: 'img',
                attribute: 'src',
            },
            // Image attachment page URL.
            imageUrl: {
                type: 'string',
                source: 'attribute',
                selector: 'a.ssl-finesse-kat-attachmenturl',
                attribute: 'href',
            },
            // Image width.
            imageWidth: {
                type: 'string',
                source: 'attribute',
                selector: 'div.ssl-finesse-kat-tab-panel',
                attribute: 'data-img-width',
            },
            // Image height.
            imageHeight: {
                type: 'string',
                source: 'attribute',
                selector: 'div.ssl-finesse-kat-tab-panel',
                attribute: 'data-img-height',
            },
		},

		supports: {
			// Removes support for editing in HTML mode.
			html: false,
        },
        
        edit: withSelect( ( select ) => {
            const { getCurrentPostId } = select( 'core/editor' );

            return {
                currentPostId: getCurrentPostId(),
            };
        } )( edit ),

        save,
        
        // May be deprecated in the future: https://github.com/WordPress/gutenberg/blob/43730e88ac819ee1b500cc3ab1f22cb173d99a34/docs/rfc/block-registration.md#backward-compatibility
        getEditWrapperProps( attributes ) {
            const { align } = attributes;

            if ( 'left' === align || 'center' === align || 'right' === align || 'wide' === align || 'full' === align ) {
                return { 'data-align': align };
            }
        },
	} );
} )(
	window.wp
);