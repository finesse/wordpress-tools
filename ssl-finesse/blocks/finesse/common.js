( function( $ ) {
    'use strict';

    // Monkeypatch older versions of jQuery to support event binding and delegation using the
    // more convenient .on() method.
    if ( ! $.fn.on ) {
        // https://github.com/georgeadamson/jQuery-on-event-binding-polyfill
        $.fn.on = function( events, selector, data, handler ) {
            var self = this;
            var args = arguments.length;

            if ( args > 3) {
                // .on(events, selector, data, handler)
                return self.delegate( selector, events, data, handler );
            } else if ( args > 2 ) {
                if ( typeof selector === 'string' ) {
                    // .on(events, selector, handler)
                    // handler = data
                    return self.delegate( selector, events, data );
                } else {
                    // .on(events, data, handler)
                    // handler = data
                    // data    = selector
                    return self.bind( events, selector, data );
                }
            } else {
                // .on(events, handler)
                // handler = selector
                return self.bind( events, selector );
            }
        }
    };

    function selectKatTabPanelTab( sourceElement, tabName ) {
        var $rootPanelDiv = sourceElement.closest( '.ssl-finesse-kat-tab-panel' );

        var $tabLinks = $rootPanelDiv.find( '.ssl-finesse-kat-tab-panel-tab-link' );
        var $targetTabLink = $rootPanelDiv.find( `.ssl-finesse-kat-tab-panel-tab-link[data-tab='${ tabName }']` );
        var $tabs = $rootPanelDiv.find( '.ssl-finesse-kat-tab-panel-tab' );
        var $targetTab = $rootPanelDiv.find( `.ssl-finesse-kat-tab-panel-tab[data-tab='${ tabName }']` );

        // Remove selected class from all tabs and tab links.
        $tabs.removeClass( 'ssl-finesse-kat-tab-panel-tab-selected' );
        $tabLinks.removeClass( 'ssl-finesse-kat-tab-panel-tab-link-selected' );
        // Add the selected class to the new selection.
        $targetTab.addClass( 'ssl-finesse-kat-tab-panel-tab-selected' );
        $targetTabLink.addClass( 'ssl-finesse-kat-tab-panel-tab-link-selected' )
    }

    // Handle clicks on dynamically created tabs.
    $( document ).on(
        'click',
        '.ssl-finesse-kat-tab-panel-tab-link',
        ( event ) => {
            var $targetTabLink = $( event.target );
            var tabName = $targetTabLink.data( 'tab' );

            selectKatTabPanelTab( $targetTabLink, tabName );
        }
    );

    // Switch to plot tab when the "Run" button is clicked.
    $( document ).on(
        'click',
        '.ssl-finesse-kat-script-input-controls-run',
        ( event ) => {
            var $targetButton = $( event.target );

            selectKatTabPanelTab( $targetButton, 'plot' );
        }
    );
} )( jQuery );
