<?php
/**
 * Standard module definitions.
 *
 * @package ssl-finesse
 */

if ( ! defined( 'WPINC' ) ) {
	// Prevent direct access.
	exit;
}

/**
 * Abstract class to define shared functions.
 */
abstract class Ssl_Finesse_Module {
	/**
	 * Parent.
	 *
	 * @var object
	 */
	protected $parent;

	/**
	 * Constructor.
	 *
	 * @param object $parent Parent class.
	 */
	public function __construct( $parent ) {
		$this->parent = $parent;
	}

	/**
	 * Get plugin name.
	 *
	 * @return string
	 */
	public function get_plugin_name() {
		return $this->parent->get_plugin_name();
	}

	/**
	 * Get loader.
	 */
	public function get_loader() {
		return $this->parent->get_loader();
	}

	/**
	 * Get version.
	 *
	 * @return string
	 */
	public function get_version() {
		return $this->parent->get_version();
	}
}
