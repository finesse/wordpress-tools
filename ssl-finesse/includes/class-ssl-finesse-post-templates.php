<?php

/**
 * Editor templates.
 *
 * @since      1.0.0
 *
 * @package    ssl-finesse
 * @subpackage ssl-finesse/includes
 */

/**
 * Editor templates.
 *
 * @package    ssl-finesse
 * @subpackage ssl-finesse/includes
 * @author     Sean Leavey <wordpress@attackllama.com>
 */
class Ssl_Finesse_Post_Templates extends Ssl_Finesse_Module {
	/**
	 * Register the filters and actions with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function register_hooks() {
        $loader = $this->get_loader();
    }
}
