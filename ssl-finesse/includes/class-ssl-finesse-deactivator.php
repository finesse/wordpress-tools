<?php

/**
 * Fired during plugin deactivation
 *
 * @since      1.0.0
 *
 * @package    ssl-finesse
 * @subpackage ssl-finesse/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    ssl-finesse
 * @subpackage ssl-finesse/includes
 * @author     Sean Leavey <wordpress@attackllama.com>
 */
class Ssl_Finesse_Deactivator {
	/**
	 * Deactivate.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}
}
