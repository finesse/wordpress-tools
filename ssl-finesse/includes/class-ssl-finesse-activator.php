<?php

/**
 * Fired during plugin activation
 *
 * @since      1.0.0
 *
 * @package    ssl-finesse
 * @subpackage ssl-finesse/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    ssl-finesse
 * @subpackage ssl-finesse/includes
 * @author     Sean Leavey <wordpress@attackllama.com>
 */
class Ssl_Finesse_Activator {
	/**
	 * Activate.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}
}
