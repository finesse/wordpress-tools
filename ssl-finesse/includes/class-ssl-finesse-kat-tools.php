<?php

/**
 * Kat script tools.
 *
 * @since      1.0.0
 *
 * @package    ssl-finesse
 * @subpackage ssl-finesse/includes
 */

/**
 * Kat script tools.
 *
 * @package    ssl-finesse
 * @subpackage ssl-finesse/includes
 * @author     Sean Leavey <wordpress@attackllama.com>
 */
class Ssl_Finesse_Kat_Tools extends Ssl_Finesse_Module {
	/**
	 * Register the filters and actions with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function register_hooks() {
		$loader = $this->get_loader();
		
		// Register REST API endpoints for running kat scripts and retrieving plots.
		$loader->add_action( 'rest_api_init', $this, 'rest_register_kat_routes' );

		// Finesse block.
        $loader->add_action( 'init', $this, 'register_finesse_block' );
    }
	
	/**
	 * Register REST API route for running kat scripts and retrieving plots.
	 */
	public function rest_register_kat_routes() {
		register_rest_route(
			SSL_FINESSE_REST_ROUTE,
			'/kat-run',
			array(
				'methods'  => array( 'POST' ),
				'callback' => array( $this, 'rest_kat_run' ),
				'args'     => array(
					'script' => array(
						'required' => true,
					),
				),
			)
		);

		register_rest_route(
			SSL_FINESSE_REST_ROUTE,
			'/kat-info',
			array(
				'methods'  => array( 'GET' ),
				'callback' => array( $this, 'rest_kat_info' ),
				'args'     => array(
					'job_id' => array(
						'required' => true,
					),
				),
			)
		);

		register_rest_route(
			SSL_FINESSE_REST_ROUTE,
			'/kat-plot',
			array(
				'methods'  => array( 'GET' ),
				'callback' => array( $this, 'rest_kat_plot' ),
				'args'     => array(
					'job_id' => array(
						'required' => true,
					),
					'post_id' => array(
						'required' => true,
					),
					'description' => array(
						'required' => false,
						'default'  => null,
					),
				),
			)
		);
	}

	/**
	 * Finesse server error.
	 * 
	 * @return WP_Error The error.
	 */
	private function finesse_server_error() {
		return new WP_Error(
			'kat_server_error',
			__( 'The Finesse server returned an invalid response.', 'ssl-finesse' ),
			array(
				'status' => 500,
			)
		);
	}

	/**
	 * Return either the specified response's body (on success, if JSON encoded) or an error.
	 * 
	 * @param WP_REST_Response $response The response.
	 * @return array|WP_Error The body, or error.
	 */
	private function get_response_or_error( $response ) {
		if ( is_wp_error( $response ) ) {
			return $response;
		}

		$decoded_body = json_decode( $response['body'] );

		if ( is_null( $decoded_body ) ) {
			return $this->finesse_server_error();
		}

		return $decoded_body;
	}

	/**
	 * Run kat script via REST API.
	 *
	 * @param WP_REST_Request $data REST request data.
	 * @return WP_REST_Response|WP_Error Response, or error if Finesse did not run or exited with an error.
	 */
	public function rest_kat_run( WP_REST_Request $data ) {
		return rest_ensure_response( $this->get_response_or_error( $this->run_kat_script( $data['script'] ) ) );
	}

	/**
	 * Run kat script.
	 * 
	 * @param string $script The kat script.
	 */
	private function run_kat_script( $script ) {
		return wp_remote_post(
			'http://legion:5000/v1/run',
			array(
				'body' => array(
					'script' => $script,
				),
			)
		);
	}

	/**
	 * Get kat run status via REST API.
	 *
	 * @param WP_REST_Request $data REST request data.
	 * @return WP_REST_Response|WP_Error Response, or error if job not found.
	 */
	public function rest_kat_info( WP_REST_Request $data ) {
		return rest_ensure_response( $this->get_response_or_error( $this->get_kat_run_info( $data['job_id'] ) ) );
	}

	/**
	 * Check kat run status.
	 * 
	 * @param string $job_id The job ID.
	 */
	private function get_kat_run_info( $job_id ) {
		$job_id = sanitize_key( $job_id );

		return wp_remote_get( "http://legion:5000/v1/job/$job_id" );
	}

	/**
	 * Plot kat results via REST API.
	 *
	 * @param WP_REST_Request $data REST request data.
	 * @return WP_REST_Response|WP_Error Response, or error if Finesse did not run or exited with an error.
	 */
	public function rest_kat_plot( WP_REST_Request $request ) {
		$attachment = $this->get_kat_plot( $request['job_id'], $request['post_id'], $request['description'] );

		if ( is_wp_error( $attachment ) ) {
			return rest_ensure_response( $attachment );
		}

		$controller = new WP_REST_Attachments_Controller( 'attachment' );
		$response = $controller->prepare_item_for_response( $attachment, $request );
		$data = $controller->prepare_response_for_collection( $response );

		return rest_ensure_response( $data );
	}

	/**
	 * Get kat plot.
	 * 
	 * This function tries to avoid uploading duplicate media by checking the remote image's
	 * ETag header against post meta in the WordPress database. If an existing attachment is
	 * matched with either of these checks, it is returned; if not, a new attachment is uploaded.
	 * 
	 * @param string $job_id      The kat job ID.
	 * @param int    $post_id     The post ID.
	 * @param string $description The image description.
	 * @return WP_Post|WP_Error The attachment, or error.
	 */
	private function get_kat_plot( $job_id, $post_id, $description = null) {
		// Get download tools.
		require_once( ABSPATH . 'wp-admin/includes/file.php' );

		$job_id = sanitize_key( $job_id );

		// Try to get SVG, if allowed.
		$filename = "plot.svg";

		$wp_filetype = wp_check_filetype( $filename );
		if ( ! $wp_filetype['ext'] && ! current_user_can( 'unfiltered_upload' ) ) {
			// Current user is not allowed to upload SVGs.
			$filename = "plot.png";
		}

		// NOTE: use add url param function to set plot settings...
		$url = "http://legion:5000/v1/job/$job_id/plot/$filename";

		$post_id = (int) $post_id;
		$job_id  = sanitize_key( $job_id );

		// FIXME: TEMPORARY! allows non-secure downloads
		add_filter( 'http_request_args', array($this, 'tmp'), 10, 2);

		// Get the remote image's ETag.
		$etag = $this->get_remote_etag( $url );

		if ( is_wp_error( $etag ) ) {
			// Return the error instead of proceding to make another request to
			// the server.
			return $etag;
		}

		if ( ! is_null( $etag ) ) {
			// Look for existing media with the job ID and ETag.
			$existing_attachment = $this->get_cached_job_media( $job_id, $etag );

			if ( ! is_null( $existing_attachment ) ) {
				return $existing_attachment;
			}
		}

		// Try to get plot.
		$attachment_id = $this->media_sideload_image(
			$url,
			// Because plots may be used in multiple posts, we set the post parent
			// to 0 to avoid attaching the image to a particular post.
			0,
			$description
		);

		if ( is_wp_error( $attachment_id ) ) {
			return $attachment_id;
		}

		$attachment = get_post( $attachment_id );

		// Set job ID as post meta.
		update_post_meta( $attachment->ID, 'ssl_finesse_kat_job_id', $job_id );

		// Get attachment's ETag. Unfortunately WordPress does not expose the
		// response data easily when using media_handle_sideload(), otherwise
		// we could get this with only one HTTP request.
		$etag = $this->get_remote_etag( $url );
		if ( ! is_null( $etag ) && ! is_wp_error( $etag ) ) {
			update_post_meta( $attachment->ID, 'ssl_finesse_kat_attachment_etag', $etag );
		}

		return $attachment;
	}

	/**
	 * Get a remote URL's ETag header, if it exists.
	 * 
	 * @param string $url The URL.
	 * @return string|WP_Error|null The ETag, or HTTP error if encountered, or null if no ETag was found.
	 */
	private function get_remote_etag( $url ) {
		$response = wp_remote_head( $url );

		if ( is_wp_error( $response ) ) {
			return $response;
		}

		$etag = wp_remote_retrieve_header( $response, 'etag' );

		if ( ! empty( $etag ) ) {
			// An ETag exists.
			return sanitize_key( $etag );
		}
	}

	/**
	 * Get attachment by job ID and ETag.
	 * 
	 * This searches post meta fields for the specified job ID and ETag. If a post matches, it is
	 * returned.
	 * 
	 * @param string $job_id The job ID to search for.
	 * @param string $etag   The ETag to search for.
	 * @return WP_Post|null The attachment, or null if no attachment found with the specified ETag.
	 */
	private function get_cached_job_media( $job_id, $etag ) {
		$posts = get_posts(
			array(
				'meta_query'     => array(
					array(
						'key'   => 'ssl_finesse_kat_job_id',
						'value' => $job_id,
					),
					array(
						'key'   => 'ssl_finesse_kat_attachment_etag',
						'value' => sanitize_key( $etag ),
					),
				),
				'post_type'      => 'attachment',
				'posts_per_page' => '1',
			)
		);

		if ( ! empty( $posts ) ) {
			return $posts[0];
		}
	}

	public function tmp( $parsed_args, $url ) {
		$unsafe = ($parsed_args['reject_unsafe_urls']) ? "true" : "false";
		error_log("reject_unsafe_urls for " . $url . " = " . $unsafe);
		$parsed_args['reject_unsafe_urls'] = false;
		return $parsed_args;
	}

	/**
	 * Downloads an image from the specified URL and attaches it to a post.
	 *
	 * This is a modified version of the core function media_handle_sideload, taken from WordPress core 5.3.0.
	 * The modifications allow images without explicit image file type extensions to be downloaded, and to always
	 * return the uploaded media ID.
	 *
	 * @param string $file    The URL of the image to download.
	 * @param int    $post_id Optional. The post ID the media is to be associated with.
	 * @param string $desc    Optional. Description of the image.
	 * @return string|WP_Error Populated HTML img tag on success, WP_Error object otherwise.
	 */
	private function media_sideload_image( $file, $post_id = 0, $desc = null ) {
		// Get required image tools.
		require_once( ABSPATH . 'wp-admin/includes/media.php' );
		require_once( ABSPATH . 'wp-admin/includes/image.php' );

		if ( empty( $file ) ) {
			return new WP_Error( 'image_sideload_failed' );
		}

		$file_array = array();

		// Set variables for storage, fix file filename for query strings.
		preg_match( '/[^\?]+\.(jpe?g|jpe|gif|png)\b/i', $file, $matches );

		if ( $matches ) {
			$file_array['name'] = wp_basename( $matches[0] );
		} else {
			$file_array['name'] = wp_basename( $file );
		}
		
		// Download file to temporary location.
		$file_array['tmp_name'] = download_url( $file );

		// If error storing temporarily, return the error.
		if ( is_wp_error( $file_array['tmp_name'] ) ) {
			return $file_array['tmp_name'];
		}

		// Do the validation and storage stuff.
		$id = media_handle_sideload( $file_array, $post_id, $desc );

		// If error storing permanently, unlink.
		if ( is_wp_error( $id ) ) {
			@unlink( $file_array['tmp_name'] );
			return $id;
		}

		return $id;
	}

    /**
     * Add Finesse block.
     * 
     * @since    1.0.0
     */
    public function register_finesse_block() {
		wp_register_script(
			'ssl-finesse-plot-script',
			esc_url( SSL_FINESSE_BASE_URL . 'blocks/finesse/common.js' ),
			array(
				'jquery',
			),
			$this->get_version(),
			true
		);

		wp_register_script(
			'ssl-finesse-plot-block-editor',
			esc_url( SSL_FINESSE_BASE_URL . 'blocks/finesse/block.js' ),
			array(
				'wp-blocks',
				'wp-components',
				'wp-editor',
				'wp-element',
				'wp-i18n',
				'wp-data',
			),
			$this->get_version(),
			true
		);
		
		wp_register_style(
			'ssl-finesse-plot-style',
			esc_url( SSL_FINESSE_BASE_URL . 'blocks/finesse/style.css' ),
			array(),
			$this->get_version()
		);
		
		wp_register_style(
			'ssl-finesse-plot-editor-style',
			esc_url( SSL_FINESSE_BASE_URL . 'blocks/finesse/editor.css' ),
			array(),
			$this->get_version()
        );
        
		register_block_type(
			'ssl-finesse/finesse-plot',
			array(
				'style'			=> 'ssl-finesse-plot-style',
				'editor_style'  => 'ssl-finesse-plot-editor-style',
				'script'		=> 'ssl-finesse-plot-script',
				'editor_script' => 'ssl-finesse-plot-block-editor',
			)
		);
    }
}
