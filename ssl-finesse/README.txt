=== Plugin Name ===
Contributors: seanleavey
Requires at least: 5.3.0
Tested up to: 5.3.0
Stable tag: 1.0.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Some useful Finesse tools for WordPress

== Description ==

This adds editor templates to posts and a Finesse block.

== Installation ==

1. Upload `ssl-finesse.php` to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress

== Changelog ==

= 1.0.0 =
* Initial release.
